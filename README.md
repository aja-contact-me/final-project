
# Final Project:   Version Control with Git

## About

Steve Byrnes teaches [Version Control with Git][course] by Atlassian on Coursea. This 4-week course introduces the Git version control system.  The course concludes with a final project assignment.  Completing the assignment requires:

* Creating and merging branches.
* Making commits with commit messages.
* Resolving merge conflicts.
* Deleting branch labels.
* Rebasing.

This Bitbucket repo hosts my final project.  After passing the course, I added images and text to this README.md file.   

## Final Project Assignment

Create​ ​a​ ​Git​ ​repository​ ​with​ ​the​ ​commits​ ​shown​ ​in​ ​the​ ​commit​ ​graph​ ​and​ ​table​ as shown below.

The commit graph and table simulate ​a​ ​team​ ​building​ ​and​ ​releasing​ ​a​ ​product​ ​using​ ​the​ ​Gitflow​ ​workflow.​ ​The​ ​first​ ​release (v1.00)​ ​has​ ​only​ ​one​ ​feature.​ ​Soon​ ​after​ ​release,​ ​a​ ​bug​ ​was​ ​discovered​ ​and​ ​a​ ​hotfix​ ​(v1.01)​ ​was necessary.

*(C) 2018 Atlassian*

![](/images/vc-git-final-proj.png)

## Tools/Platforms

I used the following tools/platforms:

* Command line (Mac)
* Bitbucket
* Figma
* VSC

## Project Steps

Steps below assume familiarity with Git and the command line.

For readers new to grouping objects and exporting in Figma, substeps breakdown #5 - 7 in more detail.


**Final Project Assingment**

1.  Create a new folder in your local repo folder named `final-proj`.
2.  From within the `final-proj` folder, create a new Git repo.
3.  Reproduce the commit graph and table from the Final Project Assignment. 
4.  Create a remote repo on Bitbucket for the project.
5.  Push all branches to Bitbucket.


**README.md Image Update**

1.  Create and checkout a new Git branch named "readme".
2.  In the `final-proj` folder, create an `images` folder.
3.  Screenshot the Final Project commit graph and table and save the screenshots in a new Figma file.  This may take multiple screenshots. 
4.  Arrange the images in Figma.  
5.  Set the background color of the Figma file to match the images (#fff). 
    1.  Click on the template background.
    2.  Under the **Design** tab located to the right of the template frame, set the **Background** to white or "FFFFFF".  The image below shows where to find the Design tab and Background setting in Figma.
    

![](/images/set-background-color.png)


6.  Group the images in Figma:
    1.  Select the arranged images.
    2.  Click **Object > Group Selection**.
7.  Export the grouped images from Figma to the `/images` folder.  Save the exported file as a PNG.
    1.  Select the grouped images.
    2.  Scroll to to the **Export** option at the bottom of the **Design** tab.
    3.  Click the **+** to expand the **Export** menu.
    4.  Click "Export Group 1" to start the export.
    5.  Before saving, set all characters in the file name to lower case.  Replace any spaces with a dash or underscore.  For example, I named my file *vc-git-final-proj.png*.
    6.  Save the file as as a PNG file in the `/images` folder.  
8.  Create a new branch named `readme`.
9.  Checkout the readme branch and open **README.md** in a code editor.  
10.  Add the following line of markdown to README.md, replacing *vc-git-final-proj.png* with your file name.  Place the Markdown line where you want the image to appear.

      `  ![](/images/vc-git-final-proj.png)  `

    For example, if you named your Figma export file "happy.png":

     `   ![](/images/**happy.png**)  `

11.  Save and close the code editor.
12.  Add the updated file to the staging area and commit with a message.
13.  Checkout the master branch.  
14.  Fast-forward merge the readme branch with the master branch.
15.  Push the master branch to the remote repo.



[course]:  https://www.coursera.org/learn/version-control-with-git/home/info